//const https = require('https');
import logo from './logo.svg';
import React from "react";
import ReactDOM from 'react-dom';
import {Link, useHistory, useParams} from 'react-router-dom';
import https from 'https';

import './App.css';

class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    const queryParams = new URLSearchParams(window.location.search);
    this.id = queryParams.get('id');
    this.state = { product : {"id":"Loading","title":"Loading","description":"the description hard coded"}};
   }

componentDidMount() {
  //  const { id } = useParams();

  const url = "/productDetails/" + this.id;
  fetch(url).then(response => response.json()).then((product) => { 
  //  console.log('here is your data', data)
    this.setState({ product: product}); 
  }
    );
}

  render() {
   // const [data, setData] = React.useState(null);
    //React.useEffect( () => {
   //   fetch("/api").then((res) => res.json()).then((data) => this.setData(data.message)).then(d => console.log(d));
    
  
//    }, []);
  //  return React.createElement('p', {}, !this.state.data ? "loading the data2!!!! " : this.state.data);
    const product = this.state.product;
    const name = <h1>Name: {product.title}</h1>; 
    const upc = <strong>Upc: {product.id}</strong>;
    const numberExternal = <p>External Id: {this.props.value}</p>;
    const description = <em>Description: {product.description}</em>;
    const picture = <img src={product.imgUrl} />
  const addCart = <button onClick={() => addToCart(product.id)}> Add to Cart</button>

    const home = <Link to="/">Back home</Link>;
    const cart = <Link to="/shoppingCart">View Shopping Cart</Link>;
  return   React.createElement('div', {}, [name, numberExternal, picture, upc, description, <br/>, addCart, home, cart]);
}

}

function addToCart(upc) {
const options = {
  path: '/addToCart/' + upc,
  method: 'PUT'
}
console.log("Trying to add to cart!");
const req = https.request(options, res => {
  console.log('statusCode: ${res.statusCode}');
});
req.on('error', error => {console.error(error) });
req.end();

}

export default ProductDetails;
