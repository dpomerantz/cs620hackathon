//const https = require('https');
import logo from './logo.svg';
import React from "react";
import ReactDOM from 'react-dom';
import {Link, useHistory, useParams} from 'react-router-dom';
import https from 'https';

import './App.css';

class ShoppingCart extends React.Component {
    constructor(props) {
      super(props);
     
    this.state = {"data" : [ ] };
     }
  
  componentDidMount() {
    //  const { id } = useParams();
  
    const url = "/shoppingCartList";
    fetch(url).then(response => response.json()).then((list) => { 
    //  console.log('here is your data', data)
      this.setState({ data: list}); 
    }
      ); 
  }
  
    render() {
    
      const array = this.state.data;
    
      const list = array.map( (str) =>
      {
         return <li>{str}</li>
      }
   );
    const header = <h1>These are the ids of what you are about to purchase!</h1>;
    const section = <ul>{list}</ul>;
    const home = <Link to="/">Back home</Link>;
    return React.createElement('div', {}, [header, section, home]);
  }
}
export default ShoppingCart;