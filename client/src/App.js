import logo from './logo.svg';
import './App.css';
import React from "react";
import ReactDOM from 'react-dom';
import {Link, BrowserRouter as Router, Route, Switch} from "react-router-dom";
import ProductDetails from './ProductDetails';
import BrowsePage from './BrowsePage';
import ShoppingCart from './ShoppingCart';

function App() {

  const router =
  <Router> 
    <Switch>
   <Route path="/" exact component={() => <BrowsePage /> } />
   <Route path="/productDetails" exact component={() => <ProductDetails value={12345}/> } />
   <Route path="/shoppingCart" exact component={() => <ShoppingCart /> } />

 </Switch>
 </Router>

;
  return ReactDOM.render(
    router,
    document.getElementById('root')
  );
}
export default App;
