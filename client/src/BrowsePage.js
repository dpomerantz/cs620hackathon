import logo from './logo.svg';
import React from "react";
import ReactDOM from 'react-dom';
import {Link, useHistory, useParams} from 'react-router-dom';

import './App.css';

class BrowsePage extends React.Component {
  constructor(props) {
    super(props);
    const queryParams = new URLSearchParams(window.location.search);
    this.id = queryParams.get('id');
  //  this.state = { product : {"id":"Loading","title":"Loading","description":"the description hard coded"}};

  this.state = {data : [ 
  
  ] 
};
  
   }

componentDidMount() {
  //  const { id } = useParams();

  const url = "/productList";
  fetch(url).then(response => response.json()).then((product) => { 
  //  console.log('here is your data', data)
    this.setState({ data: product}); 
  }
    ); 
}

  render() {
  
    const array = this.state.data;
  
    const list = array.map( (str) =>
    {
      const url = "/productDetails?id=" + str.id;
    return (
    <div>{str.title}. <Link to={url}>{str.title}  <img src={str.imgUrl} width={150} height={150}/></Link>
  
    </div>)
    
  });
  const header = <h1>Browse Page</h1>
  const section = <div>{list}</div>

  const cart = <Link to="/shoppingCart">View Shopping Cart</Link>;

  return React.createElement('div', {}, [cart, header, section]);
  
}

}

export default BrowsePage;
