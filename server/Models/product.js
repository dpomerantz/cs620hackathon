function Product(upc, description, title, imgUrl, cost) {
    return {
        "id" : upc,
        "description" : description,
        "title" : title,
        "imgUrl" : imgUrl,
        "cost" : cost
    };
};

var images = [
"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Dawson_College_04.jpg/340px-Dawson_College_04.jpg",
"https://www.incimages.com/uploaded_files/image/1920x1080/getty_655998316_2000149920009280219_363765.jpg",
"https://static01.nyt.com/images/2019/12/17/books/review/17fatbooks/17fatbooks-superJumbo.jpg",
"https://cdn.elearningindustry.com/wp-content/uploads/2016/05/top-10-books-every-college-student-read-1024x640.jpeg",
"https://static.scientificamerican.com/sciam/cache/file/1DDFE633-2B85-468D-B28D05ADAE7D1AD8_source.jpg?w=590&h=800&D80F3D79-4382-49FA-BE4B4D0C62A5C3ED"

];

function RandomProduct() {
    const id = Math.floor(Math.random() * 10000);
    return {
        "id" : id,
        "description" : "The description for " + id,
        "title" : "The title for " + id,
        "imgUrl" : images[ Math.floor(Math.random() * images.length)],
        cost: Math.random() * 100

    }
}

//export detault Product;
exports.Product = Product;
exports.RandomProduct = RandomProduct;