
const express = require("express");
const path = require('path');
var Products = require('./products.js');
var ShoppingCart = require('./shoppingCart.js');

const PORT = process.env.PORT || 3001;

const app = express();

// Have Node serve the files for our built React app
app.use(express.static(path.resolve(__dirname, '../client/build')));


app.get("/api", (req, res) => {
    res.json({ message: "Hello from server!" });
  });

  app.get("/productDetails/:upc", (req,res) => {
    const upc = req.params.upc;
    res.json(Products.productDetails(upc));
  
  });

  app.get("/productList", (req,res) => {
    const upc = req.params.upc;
    res.json(Products.productList(0, 10));
  
  });

  app.put("/addToCart/:upc", (req, res) => {
     const upc = req.params.upc;
     ShoppingCart.add(upc);
    res.json("Successfully added");
  });

  app.get("/shoppingCartList", (req, res) => {
      res.json(ShoppingCart.getList());
  })

// All other GET requests not handled before will return our React app
app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, '../client/build', 'index.html'));
  });

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});